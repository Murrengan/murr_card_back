package main

import (
	"github.com/caarlos0/env/v6"
)

// Configuration represents application configuration for serve action.
type Configuration struct {
	Host          string `env:"DATABASE_HOST" required:"true" envDefault:"localhost"`
	Port          int    `env:"DATABASE_PORT" required:"true" endDefault:"5432"`
	User          string `env:"DATABASE_USER" required:"true" envDefault:"murrcard"`
	Password      string `env:"DATABASE_PASSWORD" required:"true" envDefault:"murrcard"`
	Name          string `env:"DATABASE_NAME" required:"true" envDefault:"murrcard"`
	MigrationsDir string `env:"MIGRATIONS_DIR" envDefault:"migrations/"`
}

// LoadConfiguration returns a new application configuration parsed from environment variables.
func LoadConfiguration() (*Configuration, error) {
	var config Configuration
	if err := env.Parse(&config); err != nil {
		return nil, err
	}

	return &config, nil
}
