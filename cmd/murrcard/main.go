package main

import "gitlab.com/Murrengan/murr_card_back/internal/applications/murrcard"

func main() {
	err := murrcard.Run()
	if err != nil {
		panic(err)
	}
}
