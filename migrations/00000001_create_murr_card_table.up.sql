BEGIN;

CREATE TABLE murr_cards
(
    id         TEXT        NOT NULL,
    murren_id  TEXT        NOT NULL,
    title      TEXT        NOT NULL,
    content    TEXT        NOT NULL,
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ NOT NULL,
    dislikes   BIGINT      NOT NULL DEFAULT 0,
    likes      BIGINT      NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);

COMMIT;