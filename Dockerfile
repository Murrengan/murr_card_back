FROM golang:alpine AS builder
RUN apk update && apk add --no-cache ca-certificates git gcc make libc-dev binutils-gold
RUN mkdir murrengan
WORKDIR murrengan
COPY . .
RUN go get -d -v

RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -trimpath -o /bin/migrator ./cmd/migrate
RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -trimpath -o /bin/murrcard ./cmd/murrcard


FROM scratch
COPY --from=builder /bin/migrator /bin/migrator
COPY --from=builder /bin/murrcard /bin/murrcard
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY migrations migrations

ENV MIGRATIONS_DIR /migrations
