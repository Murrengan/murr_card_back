# MurrCard backend:

Services:
- MurrCard gRPC server
- MurrCard database migrator

### MurrCard gRPC server

- Prometheus metrics exposed on localhost:9000/metrics
- Liveliness check exposed on localhost:9000/alive

##### Launch
`docker run imagename:tagname /bin/murrcard`

- #### ENVIRONMENT VARIABLES
```dotenv
SERVER_PORT=8080 #Default: 8080
DATABASE_HOST=localhost #Default: localhost
DATABASE_PORT=5432 #Default: 5432
DATABASE_USER=murrcard #Default: murrcard
DATABASE_PASSWORD=murrcard #Default: murrcard
DATABASE_NAME=murrcard #Default: murrcard
```

### MurrCard database migrator

##### Launch
`docker run imagename:tagname /bin/migrator`

- #### ENVIRONMENT VARIABLES
```dotenv
DATABASE_HOST=localhost #Default: localhost
DATABASE_PORT=5432 #Default: 5432
DATABASE_USER=murrcard #Default: murrcard
DATABASE_PASSWORD=murrcard #Default: murrcard
DATABASE_NAME=murrcard #Default: murrcard
MIGRATIONS_DIR=migrations #Default: /migrations
```

