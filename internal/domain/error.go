package domain

import (
	"errors"
	"fmt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Error represents a generic domain error.
type Error interface {
	error

	Kind() ErrorKind
}

type ErrorKind int

func (k ErrorKind) NewError(msg string) error {
	return NewError(k, msg)
}

func (k ErrorKind) Errorf(format string, args ...interface{}) error {
	return Errorf(k, format, args...)
}

func (k ErrorKind) Is(err error) bool {
	if err == nil {
		return false
	}

	var target Error
	return errors.As(err, &target) && target.Kind() == k
}

const (
	UnknownError ErrorKind = iota + 1
	InvalidArgument
	FailedPrecondition
	NotFound
	Internal
)

func NewError(kind ErrorKind, msg string) error {
	return internalError{
		kind: kind,
		err:  errors.New(msg),
	}
}

func Errorf(kind ErrorKind, format string, args ...interface{}) error {
	return internalError{
		kind: kind,
		err:  fmt.Errorf(format, args...),
	}
}

type internalError struct {
	kind ErrorKind
	err  error
}

func (e internalError) Error() string {
	return e.err.Error()
}

func (e internalError) Kind() ErrorKind {
	return e.kind
}

func (e internalError) Is(err error) bool {
	return e.kind.Is(err) && e.Error() == err.Error()
}

func ErrorToStatus(err error) (*status.Status, bool) {
	var derr Error
	if errors.As(err, &derr) {
		var code codes.Code
		switch derr.Kind() {
		case InvalidArgument:
			code = codes.InvalidArgument
		case FailedPrecondition:
			code = codes.FailedPrecondition
		case NotFound:
			code = codes.NotFound
		case Internal:
			code = codes.Internal
		default:
			return nil, false
		}
		return status.New(code, derr.Error()), true
	}

	return nil, false
}
