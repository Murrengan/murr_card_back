package domain

import (
	"errors"
	"time"
	"unicode/utf8"
)

const (
	maxTitleLength   = 244
	maxContentLength = 1024 * 10
)

type MurrCard struct {
	ID        string    `json:"id"`
	MurrenID  string    `json:"murren_id"`
	Title     string    `json:"title"`
	Content   string    `json:"content"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Likes     int64     `json:"likes"`
	Dislikes  int64     `json:"dislikes"`
}

func (m *MurrCard) Validate() error {
	if m.Title == "" {
		return errors.New("title is empty")
	}

	if utf8.RuneCountInString(m.Title) > maxTitleLength {
		return errors.New("title is too long")
	}

	if utf8.RuneCountInString(m.Content) > maxContentLength {
		return errors.New("content is too long")
	}

	return nil
}

type VoteType string

const (
	VoteTypeLike    VoteType = "LIKE"
	VoteTypeDislike VoteType = "DISLIKE"
	VoteTypeUnknown VoteType = "UNKNOWN"
)

func (v VoteType) String() string {
	return string(v)
}
