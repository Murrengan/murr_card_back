package murrcardpbv1

import (
	murrcardpb "gitlab.com/Murrengan/murr_api/inner/murrcard/v1"
	"gitlab.com/Murrengan/murr_card_back/internal/domain"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func murrCardFromProto(card *murrcardpb.MurrCard) domain.MurrCard {
	return domain.MurrCard{
		ID:       card.GetId(),
		MurrenID: card.GetMurrenId(),
		Title:    card.GetTitle(),
		Content:  card.GetContent(),
	}
}

func murrCardToProto(card domain.MurrCard) *murrcardpb.MurrCard {
	return &murrcardpb.MurrCard{
		Id:        card.ID,
		Title:     card.Title,
		Content:   card.Content,
		CreatedAt: timestamppb.New(card.CreatedAt),
		UpdatedAt: timestamppb.New(card.UpdatedAt),
		MurrenId:  card.MurrenID,
		Likes:     card.Likes,
		Dislikes:  card.Dislikes,
	}
}

func murrCardsToProto(cards []domain.MurrCard) []*murrcardpb.MurrCard {
	murrCards := make([]*murrcardpb.MurrCard, 0, len(cards))
	for _, card := range cards {
		murrCards = append(murrCards, murrCardToProto(card))
	}
	return murrCards
}

func voteTypeFromProto(voteType murrcardpb.RateType_Enum) domain.VoteType {
	switch voteType.String() {
	case "LIKE":
		return domain.VoteTypeLike
	case "DISLIKE":
		return domain.VoteTypeDislike
	default:
		return domain.VoteTypeUnknown
	}
}
