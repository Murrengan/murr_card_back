package murrcardpbv1

import (
	"context"

	"gitlab.com/Murrengan/murr_card_back/internal/domain"
)

type MurrCardService interface {
	GetCard(ctx context.Context, id string) (domain.MurrCard, error)
	GetTotal(ctx context.Context) (int, error)
	ListCards(ctx context.Context, limit, offset int) ([]domain.MurrCard, error)
	CreateCard(ctx context.Context, card domain.MurrCard) (domain.MurrCard, error)
	UpdateCard(ctx context.Context, card domain.MurrCard) (domain.MurrCard, error)
	DeleteCard(ctx context.Context, id string) error
	RateCard(ctx context.Context, cardID string, voteType domain.VoteType) error
}
