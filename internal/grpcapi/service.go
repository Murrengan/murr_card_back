package murrcardpbv1

import (
	"context"

	murrcardpb "gitlab.com/Murrengan/murr_api/inner/murrcard/v1"
	"gitlab.com/Murrengan/murr_card_back/internal/domain"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Server struct {
	murrcardpb.UnimplementedMurrCardServiceServer

	cards MurrCardService
}

func NewServer(cards MurrCardService) *Server {
	return &Server{
		cards: cards,
	}
}

func (s *Server) GetMurrCard(ctx context.Context, request *murrcardpb.GetMurrCardRequest) (*murrcardpb.MurrCard, error) {
	card, err := s.cards.GetCard(ctx, request.GetId())
	if err != nil {
		errStatus, ok := domain.ErrorToStatus(err)
		if !ok {
			return nil, status.Error(codes.Internal, "")
		}
		return nil, status.Error(errStatus.Code(), "")
	}

	return murrCardToProto(card), nil
}

func (s *Server) ListMurrCards(ctx context.Context, request *murrcardpb.ListMurrCardsRequest) (*murrcardpb.ListMurrCardsResponse, error) {
	cards, err := s.cards.ListCards(ctx, int(request.GetLimit()), int(request.GetOffset()))
	if err != nil {
		errStatus, ok := domain.ErrorToStatus(err)
		if !ok {
			return nil, status.Error(codes.Internal, "")
		}
		return nil, errStatus.Err()
	}

	total, err := s.cards.GetTotal(ctx)
	if err != nil {
		errStatus, ok := domain.ErrorToStatus(err)
		if !ok {
			return nil, status.Error(codes.Internal, "")
		}
		return nil, errStatus.Err()
	}

	nextPageExists := request.GetLimit()+request.GetOffset() < int32(total)

	return &murrcardpb.ListMurrCardsResponse{
		MurrCards:  murrCardsToProto(cards),
		TotalCount: int32(total),
		Limit:      request.GetLimit(),
		Offset:     request.GetOffset(),
		NextPage:   nextPageExists,
	}, nil
}

func (s *Server) CreateMurrCard(ctx context.Context, request *murrcardpb.MurrCard) (*murrcardpb.MurrCard, error) {
	card := murrCardFromProto(request)

	card, err := s.cards.CreateCard(ctx, card)

	if err != nil {
		errStatus, ok := domain.ErrorToStatus(err)
		if !ok {
			return nil, status.Error(codes.Internal, "")
		}
		return nil, errStatus.Err()
	}

	return murrCardToProto(card), nil
}

func (s *Server) RateMurrCard(ctx context.Context, request *murrcardpb.RateMurrCardRequest) (*murrcardpb.RateMurrCardResponse, error) {
	cardID := request.GetMurrCardId()
	voteType := voteTypeFromProto(request.GetRate())

	err := s.cards.RateCard(ctx, cardID, voteType)

	if err != nil {
		return nil, status.Error(codes.Internal, "")
	}
	return &murrcardpb.RateMurrCardResponse{}, nil
}

func (s *Server) Register(server *grpc.Server) {
	murrcardpb.RegisterMurrCardServiceServer(server, s)
}
