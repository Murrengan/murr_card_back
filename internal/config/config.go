package config

import (
	"fmt"

	"github.com/caarlos0/env/v6"
)

type Configuration struct {
	Server   ServerConfig
	Database DatabaseConfig
}

type ServerConfig struct {
	Port int `env:"SERVER_PORT" envDefault:"8080"`
}

type DatabaseConfig struct {
	Host     string `env:"DATABASE_HOST" required:"true" envDefault:"localhost"`
	Port     int    `env:"DATABASE_PORT" required:"true" envDefault:"5432"`
	User     string `env:"DATABASE_USER" required:"true" envDefault:"murrcard"`
	Password string `env:"DATABASE_PASSWORD" required:"true" envDefault:"murrcard"`
	Name     string `env:"DATABASE_NAME" required:"true" envDefault:"murrcard"`
}

func (d *DatabaseConfig) DSN() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", d.User, d.Password, d.Host, d.Port, d.Name)
}

func LoadConfiguration() (*Configuration, error) {
	var config Configuration
	if err := env.Parse(&config); err != nil {
		return nil, err
	}
	return &config, nil
}
