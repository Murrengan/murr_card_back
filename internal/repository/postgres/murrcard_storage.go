package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/uptrace/bun"
	"gitlab.com/Murrengan/murr_card_back/internal/domain"
)

type MurrcardStorage struct {
	db *bun.DB
}

func NewMurrcardStorage(db *bun.DB) *MurrcardStorage {
	return &MurrcardStorage{db: db}
}

func (m *MurrcardStorage) GetCard(ctx context.Context, id string) (domain.MurrCard, error) {
	var model murrCardModel
	err := m.db.NewSelect().
		Model(&model).
		Where("id = ?", id).
		Scan(ctx)
	if err != nil && err != sql.ErrNoRows {
		return domain.MurrCard{}, fmt.Errorf("get card from db: %w", err)
	}
	if err == sql.ErrNoRows {
		return domain.MurrCard{}, err
	}
	return model.toDomain(), nil
}

func (m *MurrcardStorage) GetTotal(ctx context.Context) (int, error) {
	count, err := m.db.NewSelect().
		Model((*murrCardModel)(nil)).
		Count(ctx)
	if err != nil {
		return 0, fmt.Errorf("get total from db: %w", err)
	}
	return count, nil
}

func (m *MurrcardStorage) ListCards(ctx context.Context, limit, offset int) ([]domain.MurrCard, error) {
	var models []murrCardModel
	err := m.db.NewSelect().
		Model(&models).
		Limit(limit).
		Offset(offset).
		Scan(ctx)
	if err != nil {
		return nil, fmt.Errorf("list cards from db: %w", err)
	}
	cards := murrCardsToDomain(models)
	return cards, nil
}

func (m *MurrcardStorage) CreateCard(ctx context.Context, card domain.MurrCard) (domain.MurrCard, error) {
	var model murrCardModel
	model.fromDomain(card)
	_, err := m.db.NewInsert().
		Model(&model).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return domain.MurrCard{}, fmt.Errorf("create card in db: %w", err)
	}
	return model.toDomain(), nil
}

func (m *MurrcardStorage) UpdateCard(ctx context.Context, card domain.MurrCard) (domain.MurrCard, error) {
	var model murrCardModel
	model.fromDomain(card)

	allowedToUpdate := []string{"title", "content"}
	_, err := m.db.NewUpdate().
		Model(&model).
		Column(allowedToUpdate...).
		Returning("*").
		WherePK().
		Exec(ctx)

	if err != nil {
		return domain.MurrCard{}, fmt.Errorf("update card in db: %w", err)
	}
	return model.toDomain(), nil
}

func (m *MurrcardStorage) DeleteCard(ctx context.Context, id string) error {
	_, err := m.db.NewDelete().
		Model((*murrCardModel)(nil)).
		Where("id = ?", id).
		Exec(ctx)
	if err != nil {
		return fmt.Errorf("delete card from db: %w", err)
	}
	return nil
}

func (m *MurrcardStorage) LikeCard(ctx context.Context, id string) error {
	model := &murrCardModel{ID: id}

	err := m.db.RunInTx(ctx, &sql.TxOptions{}, func(ctx context.Context, tx bun.Tx) error {
		_, err := tx.NewUpdate().
			Model(model).
			Set("likes = likes + 1").
			WherePK().
			Exec(ctx)
		return err
	})
	if err != nil {
		return fmt.Errorf("upvote card in db: %w", err)
	}
	return nil
}

func (m *MurrcardStorage) DislikeCard(ctx context.Context, id string) error {
	model := &murrCardModel{ID: id}

	err := m.db.RunInTx(ctx, &sql.TxOptions{}, func(ctx context.Context, tx bun.Tx) error {
		_, err := tx.NewUpdate().
			Model(model).
			Set("dislikes = dislikes + 1").
			WherePK().
			Exec(ctx)
		return err
	})
	if err != nil {
		return fmt.Errorf("downvote card in db: %w", err)
	}
	return nil
}
