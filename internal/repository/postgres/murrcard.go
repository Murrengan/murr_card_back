package postgres

import (
	"time"

	"github.com/uptrace/bun"
	"gitlab.com/Murrengan/murr_card_back/internal/domain"
)

type murrCardModel struct {
	bun.BaseModel `bun:"table:murr_cards"`

	ID        string    `bun:"id,pk"`
	MurrenID  string    `bun:"murren_id"`
	Title     string    `bun:"title"`
	Content   string    `bun:"content"`
	CreatedAt time.Time `bun:"created_at"`
	UpdatedAt time.Time `bun:"updated_at"`
	Likes     int64     `bun:"likes"`
	Dislikes  int64     `bun:"dislikes"`
}

func (m *murrCardModel) fromDomain(murrCard domain.MurrCard) {
	m.ID = murrCard.ID
	m.MurrenID = murrCard.MurrenID
	m.Title = murrCard.Title
	m.Content = murrCard.Content
	m.CreatedAt = murrCard.CreatedAt
	m.UpdatedAt = murrCard.UpdatedAt
	m.Likes = murrCard.Dislikes
	m.Dislikes = murrCard.Dislikes
}

func (m *murrCardModel) toDomain() domain.MurrCard {
	return domain.MurrCard{
		ID:        m.ID,
		MurrenID:  m.MurrenID,
		Title:     m.Title,
		Content:   m.Content,
		CreatedAt: m.CreatedAt,
		UpdatedAt: m.UpdatedAt,
		Likes:     m.Likes,
		Dislikes:  m.Dislikes,
	}
}

func murrCardsToDomain(models []murrCardModel) []domain.MurrCard {
	murrCards := make([]domain.MurrCard, 0, len(models))
	for _, murrCard := range models {
		murrCards = append(murrCards, murrCard.toDomain())
	}

	return murrCards
}
