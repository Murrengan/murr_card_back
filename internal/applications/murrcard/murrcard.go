package murrcard

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/Murrengan/murr_card_back/internal/config"
	murrcardpbv1 "gitlab.com/Murrengan/murr_card_back/internal/grpcapi"
	"gitlab.com/Murrengan/murr_card_back/internal/repository/postgres"
	"gitlab.com/Murrengan/murr_card_back/internal/services/murrcard"
	"gitlab.com/Murrengan/murr_card_back/pkg/grpc"
	"gitlab.com/Murrengan/murr_card_back/pkg/observer"
	"gitlab.com/Murrengan/murr_card_back/pkg/server"
	"gitlab.com/Murrengan/murr_card_back/pkg/tracing"
	"go.uber.org/zap"
)

func Run() error {
	logger, err := zap.NewProduction()
	if err != nil {
		return fmt.Errorf("failed to create logger: %v", err)
	}
	defer logger.Sync() // nolint:errcheck
	appLogger := logger.Named("murrcard_application")

	cfg, err := config.LoadConfiguration()
	if err != nil {
		appLogger.Error("Error loading configuration", zap.Error(err))
		return fmt.Errorf("loading configuration: %v", err)
	}

	database := postgres.NewPostgresDatabase(cfg.Database.DSN())
	cardRepo := postgres.NewMurrcardStorage(database)
	cardService := murrcard.NewService(logger, cardRepo)

	cardServer := murrcardpbv1.NewServer(cardService)

	grpcServer := grpc.NewGrpcServer([]grpc.Server{
		cardServer,
	})

	gracefulServer := grpc.NewGracefulServer(cfg.Server.Port, grpcServer, logger)

	healthMetricsHandler := tracing.NewGracefulMetricsServer()
	metricsServer := &http.Server{
		Addr:    ":9001",
		Handler: healthMetricsHandler,
	}

	httpServer := server.NewGracefulServer(metricsServer, logger)

	obs := observer.NewObserver()

	obs.AddOpener(observer.OpenerFunc(func() error {
		return gracefulServer.Serve()
	}))

	obs.AddOpener(observer.OpenerFunc(func() error {
		return httpServer.Serve()
	}))

	obs.AddContextCloser(observer.ContextCloserFunc(func(ctx context.Context) error {
		return gracefulServer.Shutdown(ctx)
	}))

	obs.AddContextCloser(observer.ContextCloserFunc(func(ctx context.Context) error {
		return httpServer.Shutdown(ctx)
	}))

	obs.AddUpper(func(ctx context.Context) {
		select {
		case <-ctx.Done():
		case <-gracefulServer.Dead():
		case <-httpServer.Dead():
		}
	})

	return obs.Run()
}
