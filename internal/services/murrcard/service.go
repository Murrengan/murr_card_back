package murrcard

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"gitlab.com/Murrengan/murr_card_back/internal/domain"
	"go.uber.org/zap"
)

type Service struct {
	log *zap.Logger
	db  Database
}

func NewService(log *zap.Logger, db Database) *Service {
	return &Service{
		log: log.Named("murr_card_service"),
		db:  db,
	}
}

func (s *Service) GetCard(ctx context.Context, id string) (domain.MurrCard, error) {
	card, err := s.db.GetCard(ctx, id)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		s.log.Error("Error getting card", zap.Error(err))
		return domain.MurrCard{}, domain.NewError(domain.Internal, err.Error())
	}

	if errors.Is(err, sql.ErrNoRows) {
		return domain.MurrCard{}, domain.NewError(domain.NotFound, err.Error())
	}

	return card, nil
}

func (s *Service) GetTotal(ctx context.Context) (int, error) {
	total, err := s.db.GetTotal(ctx)

	if err != nil {
		s.log.Error("Error getting total", zap.Error(err))
		return 0, domain.NewError(domain.Internal, err.Error())
	}

	return total, nil
}

func (s *Service) ListCards(ctx context.Context, limit, offset int) ([]domain.MurrCard, error) {
	limit, offset = s.cleanLimitOffset(limit, offset)

	cards, err := s.db.ListCards(ctx, limit, offset)
	if err != nil {
		s.log.Error("Error getting cards", zap.Error(err))
		return nil, domain.NewError(domain.Internal, err.Error())
	}

	return cards, nil
}

func (s *Service) CreateCard(ctx context.Context, card domain.MurrCard) (domain.MurrCard, error) {
	newID := domain.NewID()
	card.ID = newID

	card.CreatedAt = time.Now().UTC()
	card.UpdatedAt = time.Now().UTC()

	newCard, err := s.db.CreateCard(ctx, card)
	if err != nil {
		s.log.Error("Error creating card", zap.Error(err))
		return domain.MurrCard{}, domain.NewError(domain.Internal, err.Error())
	}

	return newCard, nil
}

func (s *Service) UpdateCard(ctx context.Context, card domain.MurrCard) (domain.MurrCard, error) {
	card.UpdatedAt = time.Now().UTC()

	updatedCard, err := s.db.UpdateCard(ctx, card)
	if err != nil {
		s.log.Error("Error updating card", zap.Error(err))
		return domain.MurrCard{}, domain.NewError(domain.Internal, err.Error())
	}

	return updatedCard, nil
}

func (s *Service) DeleteCard(ctx context.Context, id string) error {

	err := s.db.DeleteCard(ctx, id)
	if err != nil {
		s.log.Error("Error deleting card", zap.Error(err))
		return domain.NewError(domain.Internal, err.Error())
	}

	return nil
}

func (s *Service) RateCard(ctx context.Context, cardID string, voteType domain.VoteType) error {
	switch voteType {
	case domain.VoteTypeLike:
		err := s.db.LikeCard(ctx, cardID)
		if err != nil {
			s.log.Error("like card:", zap.Error(err))
			return domain.NewError(domain.Internal, err.Error())
		}
	case domain.VoteTypeDislike:
		err := s.db.DislikeCard(ctx, cardID)
		if err != nil {
			s.log.Error("dislike card:", zap.Error(err))
			return domain.NewError(domain.Internal, err.Error())
		}
	default:
		s.log.Warn("Unknown vote type", zap.String("voteType", voteType.String()))
		return domain.NewError(domain.UnknownError, fmt.Sprintf("unknown vote type: %s", voteType.String()))
	}

	return nil
}

func (s *Service) cleanLimitOffset(limit, offset int) (int, int) {
	if limit < 0 || limit > 20 {
		limit = 20
	}
	if offset < 0 {
		offset = 0
	}
	return limit, offset
}
