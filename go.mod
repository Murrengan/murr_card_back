module gitlab.com/Murrengan/murr_card_back

go 1.16

require (
	github.com/caarlos0/env/v6 v6.9.1
	github.com/gojuno/minimock/v3 v3.0.10
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/lib/pq v1.10.4
	github.com/oklog/ulid/v2 v2.0.2
	github.com/prometheus/client_golang v1.12.1
	github.com/uptrace/bun v1.0.22
	github.com/uptrace/bun/dialect/pgdialect v1.0.22
	github.com/uptrace/bun/driver/pgdriver v1.0.22
	github.com/uptrace/bun/extra/bunotel v1.0.22
	github.com/uptrace/opentelemetry-go-extra/otelzap v0.1.8
	gitlab.com/Murrengan/murr_api v0.2.4
	go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc v0.28.0
	go.opentelemetry.io/otel v1.3.0
	go.opentelemetry.io/otel/exporters/jaeger v1.3.0
	go.opentelemetry.io/otel/sdk v1.3.0
	go.uber.org/zap v1.20.0
	golang.org/x/tools v0.1.9 // indirect
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
)
